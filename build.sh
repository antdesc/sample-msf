#!/bin/bash

#CONST VALUES
apk_name=Sample
apk_extension=.apk
apk_folder=build/apk/

#BUILD APK
chmod +x gradlew
./gradlew clean
./gradlew assembleDebug

#CREATE FILENAME
date=$(date '+%d-%m-%Y-%H%M');
apk_final_path=${apk_folder}${apk_name}_${date}${apk_extension}

#COPY FILE
mkdir -p ${apk_folder}
apk_origin_path=$(find . -type f -name '*apk' | tail -1)
cp ${apk_origin_path} ${apk_final_path}

#VOILA
