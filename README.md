# MSF Sample project

## CI

Run `sh build.sh` to generate an APK in `build/apk`

## Tests

Unit tests can be run. A simple Activity Instrumentation test has been implemented.

## Alternative image retrieval

A call to a different method in `DetailFragment` can retrieve an image from the internet. Otherwise we work with a local image.

## Todo

...

