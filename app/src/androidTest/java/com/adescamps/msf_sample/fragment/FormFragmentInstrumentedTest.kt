package com.adescamps.msf_sample.fragment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.adescamps.msf_sample.R
import com.adescamps.msf_sample.ui.view.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule

@RunWith(AndroidJUnit4::class)
class FormFragmentInstrumentedTest {
    @get:Rule
    var activityRule: ActivityScenarioRule<MainActivity>
            = ActivityScenarioRule(MainActivity::class.java)
    @Test
    fun ensureFormNominalCases() {
        val inputString = "Ad"
        val inputAngle = "50"
        onView(withId(R.id.fragment_form_string_edit_text))
            .perform(typeText(inputString), closeSoftKeyboard())
        onView(withId(R.id.fragment_form_angle_layout))
            .check(matches(isDisplayed()))
        onView(withId(R.id.fragment_form_angle_edit_text))
            .perform(typeText(inputAngle), closeSoftKeyboard())
        onView(withId(R.id.fragment_next_action_button))
            .check(matches(isClickable()))
        onView(withId(R.id.fragment_next_action_button))
            .perform(click())
        onView(withId(R.id.fragment_detail_progress_bar))
            .check(matches(isDisplayed()))
    }
}