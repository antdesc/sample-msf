package com.adescamps.msf_sample.ui.component

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.adescamps.msf_sample.R
import com.google.android.material.snackbar.Snackbar

class MessageDisplayComponentSnackbar(
    private val context: Context
): MessageDisplayComponent {
    override fun displayError(throwable: Throwable, view: View?) {
        view?.let {
            val snackbar = Snackbar.make(it, translate(throwable), Snackbar.LENGTH_SHORT)
            snackbar.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorError))
            snackbar.show()
        }
    }

    //TODO : we should do more appropriate translation errors for the appropriate exceptions (and create specific exceptions for our special cases in data layer)
    private fun translate(throwable: Throwable): String = throwable.localizedMessage ?: context.getString(R.string.error_default_message)
}