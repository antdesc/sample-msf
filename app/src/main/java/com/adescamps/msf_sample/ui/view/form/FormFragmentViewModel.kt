package com.adescamps.msf_sample.ui.view.form

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adescamps.msf_sample.data.SampleRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FormFragmentViewModel(
    private val repository: SampleRepository
) : ViewModel(), CoroutineScope by CoroutineScope(Dispatchers.Default) {

    val stringValidatorLiveData = MutableLiveData<Boolean>()
    val angleValidatorLiveData = MutableLiveData<Boolean>()

    fun checkString(input: String) {
        launch {
            stringValidatorLiveData.postValue(repository.validateString(input))
        }
    }

    fun checkAngle(input: String) {
        launch {
            angleValidatorLiveData.postValue(repository.validateAngle(input))
        }
    }
}