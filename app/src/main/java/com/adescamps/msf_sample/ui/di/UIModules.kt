package com.adescamps.msf_sample.ui.di

import com.adescamps.msf_sample.ui.component.MessageDisplayComponent
import com.adescamps.msf_sample.ui.component.MessageDisplayComponentSnackbar
import com.adescamps.msf_sample.ui.view.detail.DetailFragmentViewModel
import com.adescamps.msf_sample.ui.view.form.FormFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val applicationModule = module {
    factory<MessageDisplayComponent> { MessageDisplayComponentSnackbar(get())}
}

private val viewModelModule = module {
    viewModel { FormFragmentViewModel(get()) }
    viewModel { DetailFragmentViewModel(get()) }
}

val uiModules = arrayOf(applicationModule, viewModelModule)