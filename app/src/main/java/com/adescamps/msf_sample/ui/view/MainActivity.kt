package com.adescamps.msf_sample.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.adescamps.msf_sample.R
import com.adescamps.msf_sample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}