package com.adescamps.msf_sample.ui.component

import android.view.View

interface MessageDisplayComponent {
    fun displayError(throwable: Throwable, view: View?)
}