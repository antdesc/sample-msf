package com.adescamps.msf_sample.ui.view.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.adescamps.msf_sample.R
import com.adescamps.msf_sample.databinding.FragmentFormBinding
import com.adescamps.msf_sample.ui.utils.disable
import com.adescamps.msf_sample.ui.utils.hide
import com.adescamps.msf_sample.ui.utils.observeSafe
import com.adescamps.msf_sample.ui.utils.show
import org.koin.androidx.viewmodel.ext.android.viewModel

class FormFragment: Fragment() {
    private val viewModel: FormFragmentViewModel by viewModel()
    private lateinit var binding: FragmentFormBinding

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFormBinding.inflate(inflater, container, false)
        return binding.root
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindLiveDatas()
        initUIElements()
    }

    private fun bindLiveDatas() {
        viewModel.stringValidatorLiveData.observeSafe(viewLifecycleOwner) { success ->
            if (success) {
                binding.fragmentFormAngleLayout.show()
            } else {
                binding.fragmentFormStringEditText.error = context?.getString(R.string.error_hint_text_string)
                binding.fragmentFormAngleLayout.hide()
                binding.fragmentNextActionButton.disable()
                binding.fragmentFormAngleEditText.text?.clear()
            }
        }
        viewModel.angleValidatorLiveData.observeSafe(viewLifecycleOwner) { success ->
            binding.fragmentNextActionButton.isEnabled = success
            if (!success) {
                binding.fragmentFormAngleEditText.error = context?.getString(R.string.error_hint_text_angle)
            }
        }
    }

    private fun initUIElements() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.fragmentFormToolbar)
        binding.fragmentFormStringEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.checkString(text.toString())
        }
        binding.fragmentFormAngleEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.checkAngle(text.toString())
        }
        binding.fragmentNextActionButton.setOnClickListener {
            binding.fragmentFormAngleEditText.clearFocus()
            binding.fragmentFormStringEditText.clearFocus()
            findNavController().navigate(R.id.action_formFragment_to_detailFragment)
        }
    }
}