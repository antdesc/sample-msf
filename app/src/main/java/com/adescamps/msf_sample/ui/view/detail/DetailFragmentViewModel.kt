package com.adescamps.msf_sample.ui.view.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adescamps.msf_sample.data.SampleRepository
import com.adescamps.msf_sample.ui.utils.SingleLiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailFragmentViewModel(
    private val repository: SampleRepository
) : ViewModel(), CoroutineScope by CoroutineScope(Dispatchers.Default) {
    val errorEvent = SingleLiveEvent<Throwable>()
    val resultImageFilePath = MutableLiveData<String>()

    fun retrieveImage(resourceId: Int) {
        launch {
            repository.retrieveImage(resourceId,
                onSuccess = { result ->
                    resultImageFilePath.postValue(result)
                },
                onFailure = { throwable ->
                    errorEvent.postValue(throwable)
                })
        }
    }

    fun retrieveImageUrl() {
        val imageUrl = "https://images-cdn.9gag.com/photo/a9nXmdD_700b.jpg"
        launch {
            repository.retrieveImage(imageUrl,
                onSuccess = { result ->
                    resultImageFilePath.postValue(result)
                },
                onFailure = { throwable ->
                    errorEvent.postValue(throwable)
                })
        }
    }
}