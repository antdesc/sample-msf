package com.adescamps.msf_sample.ui

import android.app.Application
import androidx.annotation.CallSuper
import com.adescamps.msf_sample.data.di.dataModules
import com.adescamps.msf_sample.ui.di.uiModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SampleApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SampleApplication)
            modules((uiModules + dataModules).toList())
        }
    }
}