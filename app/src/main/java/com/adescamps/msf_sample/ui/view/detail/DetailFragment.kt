package com.adescamps.msf_sample.ui.view.detail

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.adescamps.msf_sample.R
import com.adescamps.msf_sample.databinding.FragmentDetailBinding
import com.adescamps.msf_sample.ui.component.MessageDisplayComponent
import com.adescamps.msf_sample.ui.utils.observeSafe
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment: Fragment() {
    private val viewModel: DetailFragmentViewModel by viewModel()
    private val displayComponent: MessageDisplayComponent by inject()
    private lateinit var binding: FragmentDetailBinding

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUIElements()
        bindLiveDatas()
        viewModel.retrieveImage(R.drawable.happy_dog)
        //viewModel.retrieveImageUrl()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                findNavController().navigateUp()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bindLiveDatas() {
        viewModel.resultImageFilePath.observeSafe(viewLifecycleOwner) { result ->
            val bitmap = BitmapFactory.decodeFile(result)
            binding.fragmentDetailImage.setImageBitmap(bitmap)
            binding.fragmentDetailProgressBar.visibility = View.GONE
            binding.fragmentDetailImage.visibility = View.VISIBLE
        }
        viewModel.errorEvent.observeSafe(viewLifecycleOwner) { throwable ->
            displayComponent.displayError(throwable, binding.fragmentDetailLayout)
            binding.fragmentDetailProgressBar.visibility = View.GONE
        }
    }

    private fun initUIElements() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.fragmentDetailToolbar)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        binding.fragmentDetailProgressBar.visibility = View.VISIBLE
        binding.fragmentDetailImage.visibility = View.GONE
    }
}