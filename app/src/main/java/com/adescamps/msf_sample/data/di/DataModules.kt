package com.adescamps.msf_sample.data.di

import com.adescamps.msf_sample.data.SampleRepository
import com.adescamps.msf_sample.data.service.DownloadService
import com.adescamps.msf_sample.data.service.DownloadServiceImpl
import com.adescamps.msf_sample.data.service.ImageService
import com.adescamps.msf_sample.data.service.ImageServiceImpl
import okhttp3.OkHttpClient
import org.koin.dsl.module


private val serviceModule = module {
    single { OkHttpClient()}
    single<ImageService> { ImageServiceImpl(get())}
    single<DownloadService> { DownloadServiceImpl(get())}
}

private val repositoryModule = module {
    single { SampleRepository(get(), get())}
}

val dataModules = arrayOf(repositoryModule, serviceModule)