package com.adescamps.msf_sample.data.service

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.Matrix
import android.net.Uri
import java.io.File
import java.io.FileInputStream

class ImageServiceImpl(
    private val context: Context
) : ImageService {
    @Throws(Throwable::class)
    override fun processImage(baseImageResourceId: Int, rotationDegrees: Int): String {
        val imageUri = Uri.Builder()
            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
            .authority(context.resources.getResourcePackageName(baseImageResourceId))
            .appendPath(context.resources.getResourceTypeName(baseImageResourceId))
            .appendPath(context.resources.getResourceEntryName(baseImageResourceId))
            .build()
        val bitmap =
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(context.contentResolver, imageUri))
        return rotateAndSaveImage(bitmap, rotationDegrees)
    }

    @Throws(Throwable::class)
    override fun processImage(baseImageFilepath: String, rotationDegrees: Int): String {
        val file = File(baseImageFilepath)
        val bitmap = BitmapFactory.decodeStream(FileInputStream(file))
        return rotateAndSaveImage(bitmap, rotationDegrees)
    }

    private fun rotateAndSaveImage(bitmap: Bitmap, rotationDegrees: Int): String {
        val matrix = Matrix().apply { postRotate(rotationDegrees.toFloat()) }
        val rotatedBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        context.openFileOutput(ImageService.DEFAULT_FILE_NAME, Context.MODE_PRIVATE).use { fos ->
            rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 0, fos)
        }
        return context.filesDir.absolutePath + ImageService.FILE_SEPARATOR + ImageService.DEFAULT_FILE_NAME
    }
}