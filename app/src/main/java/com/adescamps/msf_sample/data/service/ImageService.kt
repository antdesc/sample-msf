package com.adescamps.msf_sample.data.service


interface ImageService {
    companion object {
        const val FILE_SEPARATOR = '/'
        const val DEFAULT_FILE_NAME = "good_doggo"
    }
    fun processImage(baseImageResourceId: Int, rotationDegrees: Int): String
    fun processImage(baseImageFilepath: String, rotationDegrees: Int): String
}