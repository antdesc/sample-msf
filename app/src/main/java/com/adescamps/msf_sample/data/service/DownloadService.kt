package com.adescamps.msf_sample.data.service

interface DownloadService {
    fun downloadFile(url: String): String
}