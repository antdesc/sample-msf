package com.adescamps.msf_sample.data

import com.adescamps.msf_sample.data.service.DownloadService
import com.adescamps.msf_sample.data.service.ImageService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SampleRepository(
    private val imageService: ImageService,
    private val downloadService: DownloadService
) {
    private var cacheRotationValue = 0
    suspend fun validateString(value: String): Boolean {
        return withContext(Dispatchers.IO) {
            value.matches(Regex("^[A-Z].*\$"))
        }
    }

    suspend fun validateAngle(value: String): Boolean {
        return withContext(Dispatchers.IO) {
            try {
                val intValue = Integer.parseInt(value)
                if (intValue in 0..360) {
                    cacheRotationValue = intValue
                    true
                } else false
            } catch (e: Exception) {
                false
            }
        }
    }

    suspend fun retrieveImage(resourceId: Int, onSuccess: (rotatedImageFilepath: String) -> Unit, onFailure: (t: Throwable) -> Unit) {
        return withContext(Dispatchers.IO) {
            try {
                val rotatedImageFilepath = imageService.processImage(resourceId, cacheRotationValue)
                onSuccess(rotatedImageFilepath)
            } catch (e: Exception) {
                onFailure(e)
            }
        }
    }

    suspend fun retrieveImage(url: String, onSuccess: (rotatedImageFilepath: String) -> Unit, onFailure: (t: Throwable) -> Unit) {
        return withContext(Dispatchers.IO) {
            try {
                val downloadImageFilePath = downloadService.downloadFile(url)
                val rotatedImageFilepath = imageService.processImage(downloadImageFilePath, cacheRotationValue)
                onSuccess(rotatedImageFilepath)
            } catch (e: Exception) {
                onFailure(e)
            }
        }
    }
}