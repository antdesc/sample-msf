package com.adescamps.msf_sample.data.service

import android.accounts.NetworkErrorException
import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File
import java.net.HttpURLConnection

class DownloadServiceImpl(
    private val client: OkHttpClient
) : DownloadService {
    override fun downloadFile(url: String): String {
        val fileName = System.currentTimeMillis().toString()
        val outputFile = File.createTempFile(fileName, "")
        val request = Request.Builder().url(url).build()
        val response = client.newCall(request).execute()
        val body = response.body
        if (response.code == HttpURLConnection.HTTP_OK && body != null) {
            body.byteStream().apply {
                outputFile.outputStream().use { fileOS ->
                    copyTo(fileOS, DEFAULT_BUFFER_SIZE)
                }
            }
            return outputFile.absolutePath
        } else {
            throw NetworkErrorException()
        }
    }
}