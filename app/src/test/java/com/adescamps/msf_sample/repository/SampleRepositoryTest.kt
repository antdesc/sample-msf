package com.adescamps.msf_sample.repository

import com.adescamps.msf_sample.data.SampleRepository
import com.adescamps.msf_sample.mock.DownloadServiceMock
import com.adescamps.msf_sample.mock.ImageServiceMock
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

class SampleRepositoryTest {
    private val sampleRepository: SampleRepository = SampleRepository(ImageServiceMock(), DownloadServiceMock())
    @Before
    fun setUp() {
    }

    // String validation method
    @Test
    fun testValidateStringNominal() = runBlocking {
        assertEquals(true, sampleRepository.validateString("Das"))
        assertEquals(true, sampleRepository.validateString("A"))
        assertEquals(true, sampleRepository.validateString("Zndlsndlasndisandliasndsiladnaldinad"))
    }

    @Test
    fun testValidateStringError() = runBlocking {
        assertEquals(false, sampleRepository.validateString("das"))
        assertEquals(false, sampleRepository.validateString("a"))
        assertEquals(false, sampleRepository.validateString("000dsad"))
        assertEquals(false, sampleRepository.validateString("/*sdad"))
        assertEquals(false, sampleRepository.validateString("aDSADASDSADA"))
    }

    // Angle validation method
    @Test
    fun testValidateAngleNominal() = runBlocking {
        assertEquals(true, sampleRepository.validateAngle("99"))
        assertEquals(true, sampleRepository.validateAngle("5"))
        assertEquals(true, sampleRepository.validateAngle("250"))
    }

    @Test
    fun testValidateAngleNominalEdge() = runBlocking {
        assertEquals(true, sampleRepository.validateAngle("0"))
        assertEquals(true, sampleRepository.validateAngle("360"))
    }

    @Test
    fun testValidateAngleErrorBadInput() = runBlocking {
        assertEquals(false, sampleRepository.validateAngle("abc"))
        assertEquals(false, sampleRepository.validateAngle("1.5"))
        assertEquals(false, sampleRepository.validateAngle("/*,"))
    }

    @Test
    fun testValidateAngleErrorBadRange() = runBlocking {
        assertEquals(false, sampleRepository.validateAngle("-1"))
        assertEquals(false, sampleRepository.validateAngle("361"))
    }
}