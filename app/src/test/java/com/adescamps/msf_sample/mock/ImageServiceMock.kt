package com.adescamps.msf_sample.mock

import com.adescamps.msf_sample.data.service.ImageService

class ImageServiceMock: ImageService {
    override fun processImage(baseImageResourceId: Int, rotationDegrees: Int): String {
        throw Exception()
    }

    override fun processImage(baseImageFilepath: String, rotationDegrees: Int): String {
        throw Exception()
    }
}