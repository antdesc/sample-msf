package com.adescamps.msf_sample.mock

import com.adescamps.msf_sample.data.service.DownloadService

class DownloadServiceMock: DownloadService {
    override fun downloadFile(url: String): String {
        throw Exception()
    }
}